﻿using System;
using System.Linq;
using ValidatorLibrary;
/*
* Assignment 2 
* Cloud Application Development
* Andrew Bellamy - 215240036
* SIT323
*/
namespace RandomService
{
    public class Service : IService
    {
        private Configuration _config;

        public string GenerateAllocation(string url)
        {
            _config = new Configuration(url, true);
            string output = $"{ _getIP()}\n";
            output += _createAllocation();
            return output;
        }

        private string _getIP()
        {
            String vmPrivateIP = System.Web.HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
            return vmPrivateIP;
        }

        private string _createAllocation()
        {
            float[] disti = new float[_config.tasks.Count];
            for(short t = 0; t < _config.tasks.Count; t++)
            {
                disti[t] = _config.tasks[t].Distance;
            }
            string[] lines = new string[_config.processors.Count];
            /*Random selection of processor avoids skew by ascension*/
            Random r = new Random();
            foreach (short s in Enumerable.Range(0, _config.processors.Count).OrderBy(x => r.Next()))
                lines[s] = string.Join(",", _bruteDeeper((_config.processors[s].Frequency * _config.programMaxDuration), ref disti));
            /*Create an allocation to check for validity before handing matrix to client*/
            Allocation newAllocation = new Allocation("0", lines, _config.programTasks, _config.programProcessors);
            return newAllocation.isValid ? $"\n{string.Join("\n", lines)}\n" : "";
        }

        /*Digging into memory with pursuit pruning where there's no benefit*/
        private int[] _bruteDeeper(float capacity, ref float[] disti)
        {
            int n = disti.Length;
            bool[] optimalSelect = new bool[n];
            bool[] selection = new bool[n];
            int count = 0;
            int most = 0;
            float brute = 0;

            _dig(ref optimalSelect, selection, ref disti, 0, 0, ref brute, capacity, count, ref most);
            int[] inclusions = new int[n];
            for (short v = 0; v < n; v++)
            {
                if (optimalSelect[v])
                {
                    /*Trim this item to keep it from the next processor*/
                    disti[v] = 0;
                    inclusions[v] = 1;
                }
            }
            return inclusions;
        }

        /*Would have been better to create a utility class for this process, time permitting*/
        private void _dig(
            ref bool[] optimalSelect, 
            bool[] selection, 
            ref float[] disti, 
            int depth, 
            float run, 
            ref float brute,
            float capacity, 
            int count, 
            ref int most
        )
        {
            if (!(run <= capacity))
            {
                /*Bail out, and remove the last selection*/
                selection[depth - 1] = false;
                return;
            }
            if ((run > brute) || (run == brute && count > most))
            {
                /*Optimised by best run(distance covered) over more tasks*/
                most = count;
                brute = run;
                Array.Copy(selection, optimalSelect, selection.Length);
            }
            if (depth == selection.Length)
            {
                /*End of this verse*/
                return;
            }

            selection[depth] = false;
            _dig(ref optimalSelect, selection, ref disti, depth + 1, run, ref brute, capacity, count, ref most);

            if(disti[depth] != 0)
            {
                selection[depth] = true;
                run += disti[depth];
                ++count;
            }
            _dig(ref optimalSelect, selection, ref disti, depth + 1, run, ref brute, capacity, count, ref most);
        }

        /*Deprecated -> permutating taskset can lead to massive processing numbers*/
        private int[] _bruteFill(float capacity, ref float[] disti)
        {
            int brute = 0;
            int most = 0;
            int n = disti.Length;
            /*Possible universes in which an optimal taskset exists*/
            int possibilities = (int)Math.Pow(2, n);
            for (short s = 0; s < possibilities; s++)
            {
                float runtime = 0;
                int count = 0;
                for (short t = 0; t < n; t++)
                {
                    if (((s >> t) & 1) != 1) continue;
                    if (disti[t] == 0) continue;
                    runtime += disti[t];
                    ++count;
                }

                if (runtime <= capacity && count >= most)
                {
                    brute = s;
                    most = count;
                }
            }
            int[] inclusions = new int[n];
            for (short v = 0; v < n; v++)
            {
                Console.Write((brute >> v).ToString());
                if(((brute >> v) & 1) == 1 && disti[v] != 0)
                {
                    inclusions[v] = 1;
                    disti[v] = 0;
                }
            }
            return inclusions;
        }
    }
}
