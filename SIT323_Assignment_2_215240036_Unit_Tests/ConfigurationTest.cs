﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ValidatorLibrary;
/*
 * Assignment 2 
 * Cloud Application Development
 * Andrew Bellamy - 215240036
 * SIT323
 */
namespace SIT323_Assignment_2_215240036_Unit_Tests
{
    [TestClass]
    public class ConfigurationTest
    {
        private const short STD_ERRORS = 11; /*A Configuration instance without directives would contain these key errors*/

        /*Convenience beforeEach method for test arrangement*/
        private bool _setup(string input)
        {
            StreamWriter streamOut;
            try
            {
                streamOut = new StreamWriter($"{Environment.CurrentDirectory}\\unit.csv");
                streamOut.Write(input);
            }
            catch (IOException exc)
            {
                Debug.WriteLine(exc.Message);
                return false;
            }
            streamOut.Close();
            return true;
            /*A test csv file will be used in place of a shim or stub*/
        }


        [TestMethod]
        public void ConfigInstantiation()
        {
            Configuration testConfiguration;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("//Example Comment");
            if (_setup(sb.ToString()))
            {
                /*A Basic config instance should be invalid but != null*/
                testConfiguration = new Configuration($"{Environment.CurrentDirectory}\\unit.csv");
            } else
            {
                testConfiguration = null;
            }

            Assert.AreNotEqual(testConfiguration, null, "The configuration is undefined");
            Assert.IsFalse(testConfiguration.isValid(), "The configuration is incorrectly valid.");
            Assert.AreEqual(testConfiguration.reportErrors().Split('\n').Length, STD_ERRORS, "Number of handled errors is incorrect");
            _teardown();
        }

        [TestMethod]
        public void ConfigInvalidFilenameException()
        {
            /*No setup needed, as file shouldn't exist*/
            Assert.ThrowsException<FileNotFoundException>(() => new Configuration("invalid.csv"));
        }

        [TestMethod]
        public void ConfigInvalidLinesInCSV()
        {
            Configuration testConfiguration;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Text before comment operrand //");
            sb.AppendLine("INVALID-DIRECTIVE,BLANK");
            if (_setup(sb.ToString()))
            {
                /*A Basic config instance should be invalid but != null*/
                testConfiguration = new Configuration($"{Environment.CurrentDirectory}\\unit.csv");
            }
            else
            {
                testConfiguration = null;
            }
            /*Configuration should exist, be invalid and contain 2 handled exceptions*/
            Assert.AreNotEqual(testConfiguration, null, "The configuration is undefined");
            Assert.IsFalse(testConfiguration.isValid(), "The configuration is incorrectly valid.");
            Assert.AreEqual(testConfiguration.reportErrors().Split('\n').Length, (STD_ERRORS + 2), "Number of handled errors is incorrect");
            _teardown();
        }

        [TestMethod]
        public void ConfigPropertiesSet()
        {
            Configuration testConfiguration;
            string logfile = "test-log.txt";
            short maxDuration = 3;
            short tasks = 5;
            short processors = 3;
            short runtimeFreq = 2;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"DEFAULT-LOGFILE,{logfile}");
            sb.AppendLine($"PROGRAM-MAXIMUM-DURATION,{maxDuration}");
            sb.AppendLine($"PROGRAM-TASKS,{tasks}");
            sb.AppendLine($"PROGRAM-PROCESSORS,{processors}");
            sb.AppendLine($"RUNTIME-REFERENCE-FREQUENCY,{runtimeFreq}");
            if (_setup(sb.ToString()))
            {
                testConfiguration = new Configuration($"{Environment.CurrentDirectory}\\unit.csv");
            }
            else
            {
                testConfiguration = null;
            }
            /*Contents input into csv should match class properties*/
            Assert.AreEqual(testConfiguration.logfilename, logfile, "Logfile name is set incorrectly");
            Assert.AreEqual(testConfiguration.programMaxDuration, maxDuration, "Maximum runtime is set incorrectly");
            Assert.AreEqual(testConfiguration.programTasks, tasks, "Maximum number of tasks is set incorrectly");
            Assert.AreEqual(testConfiguration.programProcessors, processors, "Maximum number of processors is set incorrectly");
            Assert.AreEqual(testConfiguration.runtimeRefFrequency, runtimeFreq, "Runtime reference frequency is set incorrectly");
            _teardown();
        }

        [TestMethod]
        public void ConfigSequencesSet()
        {
            Configuration testConfiguration;
            short runtimeFreq = 2;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"RUNTIME-REFERENCE-FREQUENCY,{runtimeFreq}");
            sb.AppendLine("TASK-ID,RUNTIME");
            sb.AppendLine("1,1");
            sb.AppendLine("2,1");
            sb.AppendLine("3,1");
            sb.AppendLine("4,1");
            sb.AppendLine("5,1");
            sb.AppendLine();
            sb.AppendLine("PROCESSOR-ID,FREQUENCY");
            sb.AppendLine("1,1.7");
            sb.AppendLine("2,1.7");
            sb.AppendLine("3,1.7");
            sb.AppendLine();
            sb.AppendLine("COEFFICIENT-ID,VALUE");
            sb.AppendLine("0,25");
            sb.AppendLine("1,-25");
            sb.AppendLine("2,10");
            sb.AppendLine();
            if (_setup(sb.ToString()))
            {
                testConfiguration = new Configuration($"{Environment.CurrentDirectory}\\unit.csv");
            }
            else
            {
                testConfiguration = null;
            }
            /*Sequence counts should reflect csv contents*/
            Assert.AreEqual(testConfiguration.runtimeRefFrequency, runtimeFreq, "Runtime reference frequency is set incorrectly");
            Assert.AreEqual(testConfiguration.tasks.Count, 5, "Tasks sequence count is incorrect");
            Assert.AreEqual(testConfiguration.processors.Count, 3, "Processor sequence count is incorrect");
            Assert.AreEqual(testConfiguration.coefficients.Length, 3, "Coefficients sequence length is incorrect");
            _teardown();
        }

        [TestMethod]
        public void ConfigValidInstance()
        {
            Configuration testConfiguration;
            string logfile = "test-log.txt";
            short maxDuration = 3;
            short tasks = 5;
            short processors = 3;
            short runtimeFreq = 2;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"DEFAULT-LOGFILE,{logfile}");
            sb.AppendLine("LIMITS-TASKS,1,500");
            sb.AppendLine("LIMITS-PROCESSORS,1,1000");
            sb.AppendLine("LIMITS-PROCESSOR-FREQUENCIES,1,10");
            sb.AppendLine($"PROGRAM-MAXIMUM-DURATION,{maxDuration}");
            sb.AppendLine($"PROGRAM-TASKS,{tasks}");
            sb.AppendLine($"PROGRAM-PROCESSORS,{processors}");
            sb.AppendLine($"RUNTIME-REFERENCE-FREQUENCY,{runtimeFreq}");
            sb.AppendLine("TASK-ID,RUNTIME");
            sb.AppendLine("1,1");
            sb.AppendLine("2,1");
            sb.AppendLine("3,1");
            sb.AppendLine("4,1");
            sb.AppendLine("5,1");
            sb.AppendLine();
            sb.AppendLine("PROCESSOR-ID,FREQUENCY");
            sb.AppendLine("1,1.7");
            sb.AppendLine("2,1.7");
            sb.AppendLine("3,1.7");
            sb.AppendLine();
            sb.AppendLine("COEFFICIENT-ID,VALUE");
            sb.AppendLine("0,25");
            sb.AppendLine("1,-25");
            sb.AppendLine("2,10");
            sb.AppendLine();
            if (_setup(sb.ToString()))
            {
                testConfiguration = new Configuration($"{Environment.CurrentDirectory}\\unit.csv");
            }
            else
            {
                testConfiguration = null;
            }
            /*This should be a completely valid Configuration*/
            Assert.AreNotEqual(testConfiguration, null, "The configuration is undefined");
            Assert.IsTrue(testConfiguration.isValid(), "The configuration is invalid.");
            Assert.IsTrue(String.IsNullOrEmpty(testConfiguration.reportErrors()), "Handled errors exist");
            _teardown();
        }

        [TestMethod]
        public void ConfigValidInstanceErrors()
        {
            Configuration testConfiguration;
            string logfile = "test-log.txt";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"DEFAULT-LOGFILE,{logfile}");
            sb.AppendLine("LIMITS-TASKS,A,A");
            sb.AppendLine("LIMITS-PROCESSORS,A,A");
            sb.AppendLine("LIMITS-PROCESSOR-FREQUENCIES,A,A");
            sb.AppendLine($"PROGRAM-MAXIMUM-DURATION,A");
            sb.AppendLine($"PROGRAM-TASKS,A");
            sb.AppendLine($"PROGRAM-PROCESSORS,A");
            sb.AppendLine($"RUNTIME-REFERENCE-FREQUENCY,A");
            sb.AppendLine("TASK-ID,RUNTIME");
            sb.AppendLine("1,1");
            sb.AppendLine("2,1");
            sb.AppendLine("3,1");
            sb.AppendLine("4,1");
            sb.AppendLine("5,1");
            sb.AppendLine();
            sb.AppendLine("PROCESSOR-ID,FREQUENCY");
            sb.AppendLine("1,1.7");
            sb.AppendLine("2,1.7");
            sb.AppendLine("3,1.7");
            sb.AppendLine();
            sb.AppendLine("COEFFICIENT-ID,VALUE");
            sb.AppendLine("0,A");
            sb.AppendLine("1,A");
            sb.AppendLine("2,A");
            sb.AppendLine();
            if (_setup(sb.ToString()))
            {
                testConfiguration = new Configuration($"{Environment.CurrentDirectory}\\unit.csv");
            }
            else
            {
                testConfiguration = null;
            }
            Assert.AreNotEqual(testConfiguration, null, "The configuration is undefined");
            Assert.IsFalse(testConfiguration.isValid(), "The configuration is incorrectly valid.");
            Assert.IsFalse(String.IsNullOrEmpty(testConfiguration.reportErrors()), "Errors haven't been written to log");
            _teardown();
        }

        /*Teardown for after each test. Ensures clean test environment*/
        private static void _teardown()
        {
            if (File.Exists($"{Environment.CurrentDirectory}\\unit.csv")) File.Delete($"{Environment.CurrentDirectory}\\unit.csv");
            if (File.Exists($"{Environment.CurrentDirectory}\\test-log.txt")) File.Delete($"{Environment.CurrentDirectory}\\test-log.txt");
        }
    }
}
