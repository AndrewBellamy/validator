﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ValidatorLibrary;
/*
 * Assignment 2 
 * Cloud Application Development
 * Andrew Bellamy - 215240036
 * SIT323
 */
namespace SIT323_Assignment_2_215240036_Unit_Tests
{
    [TestClass]
    public class TaskTest
    {
        [TestMethod]
        public void TaskInstantiation()
        {
            short runtimeRefFrequency = 2;
            short id = 1;
            short runtime = 1;
            string line = $"{id},{runtime}";

            Task testTask = new Task(line, runtimeRefFrequency);

            Assert.AreNotEqual(testTask, null, "The Task is undefined");
            Assert.AreEqual(testTask.Distance, (runtime * runtimeRefFrequency), "Calculated distance is incorrect");
        }

        [TestMethod]
        public void TaskInvalidConstructionException()
        {
            short runtimeRefFrequency = 2;
            /*Obviously a null or empty string should throw an exception*/
            Assert.ThrowsException<Exception>(() => new Task("", runtimeRefFrequency));
        }

        [TestMethod]
        public void TaskInvalidDataTypeException()
        {
            /*These values should throw an exception*/
            Assert.ThrowsException<Exception>(() => new Task("1.2,A", -4));
        }
    }
}
