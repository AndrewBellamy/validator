﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ValidatorLibrary;
/*
 * Assignment 2 
 * Cloud Application Development
 * Andrew Bellamy - 215240036
 * SIT323
 */
namespace SIT323_Assignment_2_215240036_Unit_Tests
{
    [TestClass]
    public class ProcessorTest
    {
        [TestMethod]
        public void ProcessorInstantiation()
        {
            /*Checks the creation of a processor object*/
            short id = 1;
            float frequency = 1.23F;
            string testLine = $"{id},{frequency:F2}";
            Processor testProcessor = new Processor(testLine);

            Assert.AreNotEqual(testProcessor, null, "The processor is undefined");
            Assert.AreEqual(testProcessor.ID, id, "Identifier does not match");
            Assert.AreEqual(testProcessor.Frequency, frequency, "Frequency does not match");
        }

        [TestMethod]
        public void ProcessorInvalidConstructionException()
        {
            /*Obviously a null or empty string should throw an exception*/
            Assert.ThrowsException<Exception>(() => new Processor(""));
        }

        [TestMethod]
        public void ProcessorInvalidDataTypeException()
        {
            /*These values should throw an exception*/
            Assert.ThrowsException<Exception>(() => new Processor("1.2,A"));
        }
    }
}
