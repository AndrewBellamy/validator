﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ValidatorLibrary;
/*
 * Assignment 2 
 * Cloud Application Development
 * Andrew Bellamy - 215240036
 * SIT323
 */
namespace SIT323_Assignment_2_215240036_Unit_Tests
{
    [TestClass]
    public class AllocationTest
    {
        [TestMethod]
        public void AllocationInstantiation()
        {
            string id = "0";
            string[] lines = new string[] { "1,1,0,0,0", "0,0,1,1,0", "0,0,0,0,1" };
            short maxTasks = 5;
            short maxProcessors = 3;

            Allocation testAllocation = new Allocation(id, lines, maxTasks, maxProcessors);

            Assert.AreNotEqual(testAllocation, null, "The Allocation is undefined");
            Assert.AreEqual(testAllocation.ID, id, "0");
            Assert.IsTrue(testAllocation.isValid);
            Assert.IsTrue(String.IsNullOrEmpty(testAllocation.reportErrors()));
        }

        [TestMethod]
        public void AllocationInvalidReadLinesException()
        {
            string id = "0";
            string[] lines = new string[] { "", "", "" };
            short maxTasks = 5;
            short maxProcessors = 3;
            /*Empty lines should throw an exception*/
            Assert.ThrowsException<Exception>(() => new Allocation(id, lines, maxTasks, maxProcessors));
        }

        [TestMethod]
        public void AllocationInvalidDataException()
        {
            string id = "0";
            string[] lines = new string[] { "A,1,0,0,0", "B,0,1,1,0", "C,0,0,0,1" };
            short maxTasks = 5;
            short maxProcessors = 3;
            /*Empty lines should throw an exception*/
            Assert.ThrowsException<Exception>(() => new Allocation(id, lines, maxTasks, maxProcessors));
        }

        [TestMethod]
        public void AllocationDualAllocationError()
        {
            string id = "0";
            /*Task 3 (index 2) should write an error to the invalidity report*/
            string[] lines = new string[] { "1,1,1,0,0", "0,0,1,1,0", "0,0,0,0,1" };
            short maxTasks = 5;
            short maxProcessors = 3;

            Allocation testAllocation = new Allocation(id, lines, maxTasks, maxProcessors);

            Assert.AreNotEqual(testAllocation, null, "The Allocation is undefined");
            Assert.AreEqual(testAllocation.ID, id, "0");
            Assert.IsFalse(testAllocation.isValid);
            Assert.IsFalse(String.IsNullOrEmpty(testAllocation.reportErrors()));
        }

        [TestMethod]
        public void AllocationNoAllocationError()
        {
            string id = "0";
            /*Task 3 (index 2) should write an error to the invalidity report*/
            string[] lines = new string[] { "1,1,1,0,0", "0,0,1,1,0", "0,0,0,0,0" };
            short maxTasks = 5;
            short maxProcessors = 3;

            Allocation testAllocation = new Allocation(id, lines, maxTasks, maxProcessors);

            Assert.AreNotEqual(testAllocation, null, "The Allocation is undefined");
            Assert.AreEqual(testAllocation.ID, id, "0");
            Assert.IsFalse(testAllocation.isValid);
            Assert.IsFalse(String.IsNullOrEmpty(testAllocation.reportErrors()));
        }
    }
}
