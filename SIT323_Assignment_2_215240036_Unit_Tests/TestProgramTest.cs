﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SIT323_Assignment_2_215240036;
/*
 * Assignment 2 
 * Cloud Application Development
 * Andrew Bellamy - 215240036
 * SIT323
 */
namespace SIT323_Assignment_2_215240036_Unit_Tests
{
    [TestClass]
    public class TestProgramTest
    {
        /*Convenience beforeEach method for test arrangement*/
        private bool _setup(string input)
        {
            StreamWriter streamOut;
            try
            {
                streamOut = new StreamWriter($"{Environment.CurrentDirectory}\\unit.tan");
                streamOut.Write(input);
            }
            catch (IOException exc)
            {
                Debug.WriteLine(exc.Message);
                return false;
            }
            streamOut.Close();
            return true;
            /*A test tan file will be used in place of a shim or stub*/
        }

        private bool _createConfiguration()
        {
            string logfile = "test-log.txt";
            short maxDuration = 3;
            short tasks = 5;
            short processors = 3;
            short runtimeFreq = 2;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"DEFAULT-LOGFILE,{logfile}");
            sb.AppendLine("LIMITS-TASKS,1,500");
            sb.AppendLine("LIMITS-PROCESSORS,1,1000");
            sb.AppendLine("LIMITS-PROCESSOR-FREQUENCIES,1,10");
            sb.AppendLine($"PROGRAM-MAXIMUM-DURATION,{maxDuration}");
            sb.AppendLine($"PROGRAM-TASKS,{tasks}");
            sb.AppendLine($"PROGRAM-PROCESSORS,{processors}");
            sb.AppendLine($"RUNTIME-REFERENCE-FREQUENCY,{runtimeFreq}");
            sb.AppendLine("TASK-ID,RUNTIME");
            sb.AppendLine("1,1");
            sb.AppendLine("2,1");
            sb.AppendLine("3,1");
            sb.AppendLine("4,1");
            sb.AppendLine("5,1");
            sb.AppendLine();
            sb.AppendLine("PROCESSOR-ID,FREQUENCY");
            sb.AppendLine("1,1.7");
            sb.AppendLine("2,2.3");
            sb.AppendLine("3,1.7");
            sb.AppendLine();
            sb.AppendLine("COEFFICIENT-ID,VALUE");
            sb.AppendLine("0,25");
            sb.AppendLine("1,-25");
            sb.AppendLine("2,10");
            sb.AppendLine();
            StreamWriter streamOut;
            try
            {
                streamOut = new StreamWriter($"{Environment.CurrentDirectory}\\unit.csv");
                streamOut.Write(sb.ToString());
            }
            catch (IOException exc)
            {
                Debug.WriteLine(exc.Message);
                return false;
            }
            streamOut.Close();
            return true;
        }

        [TestMethod]
        public void TestProgramInstantiation()
        {
            TestProgram testProgram;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("//Example Comment");
            if (_setup(sb.ToString()))
            {
                testProgram = new TestProgram(new FileStream($"{Environment.CurrentDirectory}\\unit.tan", FileMode.Open));
            }
            else
            {
                testProgram = null;
            }
            Assert.AreNotEqual(testProgram, null, "The TestProgram is undefined");
            Assert.IsFalse(testProgram.isValid, "The TestProgram is incorrectly valid");
            _teardown();
        }

        [TestMethod]
        public void TestProgramValidInstance()
        {
            TestProgram testProgram;
            string toStringResult = "";
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CONFIGURATION,\"unit.csv\"");
            sb.AppendLine("TASKS,5");
            sb.AppendLine("PROCESSORS,3");
            sb.AppendLine("ALLOCATIONS,1");
            sb.AppendLine("ALLOCATION-ID,1");
            sb.AppendLine("1,1,0,0,0");
            sb.AppendLine("0,0,1,1,0");
            sb.AppendLine("0,0,0,0,1");
            sb.AppendLine();
            if (_setup(sb.ToString()) && _createConfiguration())
            {
                testProgram = new TestProgram(new FileStream($"{Environment.CurrentDirectory}\\unit.tan", FileMode.Open));
                toStringResult = testProgram.ToString();
            }
            else
            {
                testProgram = null;
            }
            Assert.AreNotEqual(testProgram, null, "The TestProgram is undefined");
            Assert.IsTrue(testProgram.isValid, "The TestProgram is incorrectly invalid");
            /*Trim and evaluate string method, as we buffer the errors with newlines*/
            Assert.IsTrue(String.IsNullOrWhiteSpace(testProgram.reportErrors()), "There are errors in the report, though the program is valid?");
            Assert.IsTrue(!String.IsNullOrWhiteSpace(toStringResult), "ToString method should always return a result.");
            _teardown();
        }

        [TestMethod]
        public void TestProgramInvalidTasks()
        {
            TestProgram testProgram;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CONFIGURATION,\"unit.csv\"");
            sb.AppendLine("TASKS,A"); /*<- This should throw an error in each task, plus a parse error*/
            sb.AppendLine("PROCESSORS,3");
            sb.AppendLine("ALLOCATIONS,1");
            sb.AppendLine("ALLOCATION-ID,1");
            sb.AppendLine("1,1,0,0,0");
            sb.AppendLine("0,0,1,1,0");
            sb.AppendLine("0,0,0,0,1");
            sb.AppendLine();
            if (_setup(sb.ToString()) && _createConfiguration())
            {
                testProgram = new TestProgram(new FileStream($"{Environment.CurrentDirectory}\\unit.tan", FileMode.Open));
            }
            else
            {
                testProgram = null;
            }
            Assert.AreNotEqual(testProgram, null, "The TestProgram is undefined");
            Assert.IsFalse(testProgram.isValid, "The TestProgram is incorrectly valid");
            _teardown();
        }

        [TestMethod]
        public void TestProgramInvalidProcessors()
        {
            TestProgram testProgram;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CONFIGURATION,\"unit.csv\"");
            sb.AppendLine("TASKS,5");
            sb.AppendLine("PROCESSORS,A"); /*<- This should throw an error in each processor, plus a parse error*/
            sb.AppendLine("ALLOCATIONS,1");
            sb.AppendLine("ALLOCATION-ID,1");
            sb.AppendLine("1,1,0,0,0");
            sb.AppendLine("0,0,1,1,0");
            sb.AppendLine("0,0,0,0,1");
            sb.AppendLine();
            if (_setup(sb.ToString()) && _createConfiguration())
            {
                testProgram = new TestProgram(new FileStream($"{Environment.CurrentDirectory}\\unit.tan", FileMode.Open));
            }
            else
            {
                testProgram = null;
            }
            Assert.AreNotEqual(testProgram, null, "The TestProgram is undefined");
            Assert.IsFalse(testProgram.isValid, "The TestProgram is incorrectly valid");
            _teardown();
        }

        [TestMethod]
        public void TestProgramInvalidAllocations()
        {
            TestProgram testProgram;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CONFIGURATION,\"unit.csv\"");
            sb.AppendLine("TASKS,5");
            sb.AppendLine("PROCESSORS,3"); 
            sb.AppendLine("ALLOCATIONS,A"); /*<- This should throw a parse error, and an error for the allocation sequence*/
            sb.AppendLine("ALLOCATION-ID,1");
            sb.AppendLine("1,1,0,0,0");
            sb.AppendLine("0,0,1,1,0");
            sb.AppendLine("0,0,0,0,1");
            sb.AppendLine();
            if (_setup(sb.ToString()) && _createConfiguration())
            {
                testProgram = new TestProgram(new FileStream($"{Environment.CurrentDirectory}\\unit.tan", FileMode.Open));
            }
            else
            {
                testProgram = null;
            }
            Assert.AreNotEqual(testProgram, null, "The TestProgram is undefined");
            Assert.IsFalse(testProgram.isValid, "The TestProgram is incorrectly valid");
            _teardown();
        }

        [TestMethod]
        public void TestProgramUnmatchedIndexAllocations()
        {
            TestProgram testProgram;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CONFIGURATION,\"unit.csv\"");
            sb.AppendLine("TASKS,5");
            sb.AppendLine("PROCESSORS,3");
            sb.AppendLine("ALLOCATIONS,2"); /*<- Throw in an index error check for the purpose of regression*/
            sb.AppendLine("ALLOCATION-ID,1");
            sb.AppendLine("1,1,0,0,0");
            sb.AppendLine("0,0,1,1,0");
            sb.AppendLine("0,0,0,0,1");
            sb.AppendLine();
            if (_setup(sb.ToString()) && _createConfiguration())
            {
                testProgram = new TestProgram(new FileStream($"{Environment.CurrentDirectory}\\unit.tan", FileMode.Open));
            }
            else
            {
                testProgram = null;
            }
            Assert.AreNotEqual(testProgram, null, "The TestProgram is undefined");
            Assert.IsFalse(testProgram.isValid, "The TestProgram is incorrectly valid");
            _teardown();
        }

        [TestMethod]
        public void TestProgramExcessiveTimeError()
        {
            TestProgram testProgram;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CONFIGURATION,\"unit.csv\"");
            sb.AppendLine("TASKS,5");
            sb.AppendLine("PROCESSORS,3");
            sb.AppendLine("ALLOCATIONS,1");
            sb.AppendLine("ALLOCATION-ID,1");
            sb.AppendLine("0,0,0,0,0");
            sb.AppendLine("1,1,1,1,1"); /*<- This will tip the runtime over maximum*/
            sb.AppendLine("0,0,0,0,0");
            sb.AppendLine();
            if (_setup(sb.ToString()) && _createConfiguration())
            {
                testProgram = new TestProgram(new FileStream($"{Environment.CurrentDirectory}\\unit.tan", FileMode.Open));
                testProgram.ToString();
            }
            else
            {
                testProgram = null;
            }
            Assert.AreNotEqual(testProgram, null, "The TestProgram is undefined");
            Assert.IsFalse(testProgram.isValid, "The TestProgram is incorrectly valid");
            _teardown();
        }

        [TestMethod]
        public void TestProgramAllocationEnergy()
        {
            TestProgram testProgram;
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("CONFIGURATION,\"unit.csv\"");
            sb.AppendLine("TASKS,5");
            sb.AppendLine("PROCESSORS,3");
            sb.AppendLine("ALLOCATIONS,2");
            sb.AppendLine("ALLOCATION-ID,1");
            sb.AppendLine("1,1,0,0,0");
            sb.AppendLine("0,0,1,1,0");
            sb.AppendLine("0,0,0,0,1");
            sb.AppendLine();
            sb.AppendLine("ALLOCATION-ID,2"); /*<- this energy should exceed the previous*/
            sb.AppendLine("1,0,0,0,0");
            sb.AppendLine("0,1,1,1,0");
            sb.AppendLine("0,0,0,0,1");
            sb.AppendLine();
            if (_setup(sb.ToString()) && _createConfiguration())
            {
                testProgram = new TestProgram(new FileStream($"{Environment.CurrentDirectory}\\unit.tan", FileMode.Open));
                testProgram.ToString();
            }
            else
            {
                testProgram = null;
            }
            Assert.AreNotEqual(testProgram, null, "The TestProgram is undefined");
            Assert.IsFalse(testProgram.isValid, "The TestProgram is incorrectly valid");
            _teardown();
        }

        /*Teardown for after each test. Ensures clean test environment*/
        private static void _teardown()
        {
            if (File.Exists($"{Environment.CurrentDirectory}\\unit.tan")) File.Delete($"{Environment.CurrentDirectory}\\unit.tan");
            if (File.Exists($"{Environment.CurrentDirectory}\\unit.csv")) File.Delete($"{Environment.CurrentDirectory}\\unit.csv");
            if (File.Exists($"{Environment.CurrentDirectory}\\test-log.txt")) File.Delete($"{Environment.CurrentDirectory}\\test-log.txt");
        }
    }
}
