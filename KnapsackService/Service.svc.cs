﻿using System;
using System.Collections.Generic;
using System.Linq;
using ValidatorLibrary;
/*
* Assignment 2 
* Cloud Application Development
* Andrew Bellamy - 215240036
* SIT323
*/
namespace KnapsackService
{
    public class Service : IService
    {
        private Configuration _config;

        public string GenerateAllocation(string url)
        {
            _config = new Configuration(url, true);
            string output = $"{ _getIP()}\n";
            output += _createAllocation();
            return output;
        }

        private string _getIP()
        {
            String vmPrivateIP = System.Web.HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
            return vmPrivateIP;
        }

        private string _createAllocation()
        {
            /*Dictionary map of tasks to be zeroed out when selected*/
            Dictionary<int, float> availableTasks = new Dictionary<int, float>();
            for (short t = 0; t < _config.tasks.Count; t++)
            {
                availableTasks.Add(t, _config.tasks[t].Distance);
            }
            string[] lines = new string[_config.processors.Count];
            /*Random selection of processor avoids skew by ascension*/
            Random r = new Random();
            foreach (short s in Enumerable.Range(0, _config.processors.Count).OrderBy(x => r.Next()))
                lines[s] = string.Join(",", _coinChangeReducer((_config.processors[s].Frequency * _config.programMaxDuration), ref availableTasks));
            /*Create an allocation to check for validity before handing matrix to client*/
            Allocation newAllocation = new Allocation("0", lines, _config.programTasks, _config.programProcessors);
            return newAllocation.isValid ? $"\n{string.Join("\n", lines)}\n" : "";
        }

        /*Based on the coin change problem paradigm*/
        private int[] _coinChangeReducer(float amount, ref Dictionary<int, float> availableTasks)
        { 
            int[] primary = new int[availableTasks.Count];
            int[] secondary = new int[availableTasks.Count];

            float least = 0 + amount;
            int[] inclusions = (Change(amount, availableTasks, primary, ref secondary, ref least) != null) ? primary : secondary;
            
            /*Once we've decided what will be included, we'll zero the values from the set*/
            for(short s = 0; s < inclusions.Length; s++)
            {
                if (inclusions[s] == 1) availableTasks[s] = 0;
            }
            return inclusions;
	    }

        private static Dictionary<int, float> Change(float amount, Dictionary<int, float> availableTasks, int[] primary, ref int[] secondary, ref float least)
        {
            /*Once we reach the end, bubble up*/
            if (amount == 0) return availableTasks;
            if (amount < 0) return null;

            /*Coin change problem doesn't think about closest match, this should yield second best fit*/
            if (amount < least)
            {
                least = amount;
                for(short s = 0; s < secondary.Length; s++)
                {
                    secondary[s] = availableTasks.ContainsKey(s) ? 0 : 1;
                }
            }

            /*Recursive attempts to better the remaining value as we remove tasks*/
            foreach (var availableTask in availableTasks.Where(at => amount >= at.Value && at.Value != 0))
            {
                var newAvailableTasks = new Dictionary<int, float>(availableTasks);
                newAvailableTasks.Remove(availableTask.Key);
                var change = Change(amount - availableTask.Value, newAvailableTasks, primary, ref secondary, ref least);

                if (change == newAvailableTasks)
                {
                    primary[availableTask.Key] = 1;         
                    return availableTasks;
                }
            }
            
            return null;
        }
    }
}
