﻿using System.ServiceModel;
/*
* Assignment 2 
* Cloud Application Development
* Andrew Bellamy - 215240036
* SIT323
*/
namespace AllocationService
{
    [ServiceContract]
    public interface IService
    {

        [OperationContract]
        string GenerateAllocation(string url);

    }
}
