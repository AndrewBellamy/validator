﻿using System;
using System.Linq;
using ValidatorLibrary;
/*
* Assignment 2 
* Cloud Application Development
* Andrew Bellamy - 215240036
* SIT323
*/
namespace AllocationService
{
    public class Service : IService
    {
        private Configuration _config;

        public string GenerateAllocation(string url)
        {
            _config = new Configuration(url, true);
            string output = $"{ _getIP()}\n";
            output += _createAllocation();
            return output;
        }

        private string _getIP()
        {
            String vmPrivateIP = System.Web.HttpContext.Current.Request.ServerVariables["LOCAL_ADDR"];
            return vmPrivateIP;
        }

        private string _createAllocation()
        {
            float[,] results = new float[_config.processors.Count, (_config.tasks.Count + 1)];
            int upperbound = _config.tasks.Count + 1;
            Random r = new Random();
            foreach (short s in Enumerable.Range(1, _config.tasks.Count).OrderBy(x => r.Next())) _processTask(results, s, 0);
            string[] lines = new string[_config.processors.Count];
            for (short s = 0; s < _config.processors.Count; s++) lines[s] =  string.Join(",", results.GetAllocationTuple(s).ToArray());
            /*Create an allocation to check for validity before handing matrix to client*/
            Allocation newAllocation = new Allocation("0", lines, _config.programTasks, _config.programProcessors);
            return newAllocation.isValid ? $"\n{string.Join("\n", lines)}\n" : "";
        }

        /*Random task handed to greed processor allocation*/
        private void _processTask(float[,] matrix, short task, int processor)
        {
            if (processor >= _config.processors.Count) return;
            float runtime = _config.tasks[(task - 1)].Distance / _config.processors[processor].Frequency;
            if ((matrix[processor, 0] + runtime) < _config.programMaxDuration)
            {
                matrix[processor, task] = 1;
                matrix[processor, 0] += runtime;
                return;
            } else
            {
                _processTask(matrix, task, (processor + 1));
            }
        }
    }
}
