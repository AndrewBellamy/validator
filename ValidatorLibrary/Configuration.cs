﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
/*
* Assignment 2 
* Cloud Application Development
* Andrew Bellamy - 215240036
* SIT323
*/
namespace ValidatorLibrary
{
    public class Configuration
    {
        private const short NUM_COEFFICIENT = 3;
        private const short LIMIT_CAPACITY = 2;
        /*Properties*/
        private string _logfileName = ""; /*Avoid null reference*/
        private int[] _limitTasks;
        private int[] _limitProcessors;
        private float[] _limitFrequencies;
        private float _programMaxDuration;
        private short _programTasks;
        private short _programProcessors;
        private float _runtimeRefFrequency;
        private List<Task> _tasks;
        private List<Processor> _processors;
        private float[] _coefficients;
        private string _invalidityReport;
        /*Public read-only accessors*/
        public string logfilename { get => _logfileName; }
        public float programMaxDuration { get => _programMaxDuration; }
        public short programTasks { get => _programTasks; }
        public short programProcessors { get => _programProcessors; }
        public List<Task> tasks { get => _tasks; }
        public List<Processor> processors { get => _processors; }
        public float[] coefficients { get => _coefficients; }
        public float runtimeRefFrequency { get => _runtimeRefFrequency; }

        /*Constructor*/
        public Configuration(string filepath, bool online = false)
        {
            StreamReader configurationStream;
            try
            {
                if (online)
                {
                    configurationStream = _downloadFile(filepath);
                }
                else
                {
                    configurationStream = _openFile(filepath);
                }
            }
            catch (IOException exc)
            {
                throw exc;
            }
            /*Creation doesn't occur if the first isn't satisfied*/
            _tasks = new List<Task>();
            _processors = new List<Processor>();
            _coefficients = new float[NUM_COEFFICIENT];
            _setupConfiguration(configurationStream);
            /*Finally, validate for missing keys*/
            _validateConfig();
            /*Free-up system resource*/
            configurationStream.Close();
        }
        private StreamReader _downloadFile(string filepath)
        {
            WebClient wc = new WebClient();
            Stream configurationfile;
            try
            {
                configurationfile = wc.OpenRead(filepath);
            }
            catch (Exception exc)
            {
                throw exc;
            }
            return new StreamReader(configurationfile);
        }
        /*Opens the filestream, using test program configuration key. Sets the directory filepath for output.*/
        private StreamReader _openFile(string filepath)
        {
            FileStream configurationfile;
            try
            {
                configurationfile = new FileStream(filepath, FileMode.Open);
            }
            catch (IOException exc)
            {
                throw exc;
            }
            return new StreamReader(configurationfile);
        }
        /*Performs the legwork for building the configuration object*/
        private void _setupConfiguration(StreamReader configurationStream)
        {
            int count = 0;
            while (!configurationStream.EndOfStream)
            {
                string currentline = configurationStream.ReadLine();
                try
                {
                    string[] commands = currentline.Split(',');
                    switch (commands[0])
                    {
                        case "DEFAULT-LOGFILE":
                            if (!String.IsNullOrWhiteSpace(_logfileName)) throw new Exception("Key 'DEFAULT-LOGFILE' has already been set.");
                            _logfileName = commands[1].Replace("\"", ""); /*Filenames are stringified*/
                            break;
                        case "LIMITS-TASKS":
                            _setLimitTasks(commands[1], commands[2]);
                            break;
                        case "LIMITS-PROCESSORS":
                            _setLimitProcessors(commands[1], commands[2]);
                            break;
                        case "LIMITS-PROCESSOR-FREQUENCIES":
                            _setLimitFrequencies(commands[1], commands[2]);
                            break;
                        case "PROGRAM-MAXIMUM-DURATION":
                            if (_programMaxDuration != default(float)) throw new Exception("Key 'PROGRAM-MAXIMUM-DURATION' has already been set.");
                            _programMaxDuration = _convertProgramSetting(commands[1]);
                            break;
                        case "PROGRAM-TASKS":
                            if (_programTasks != default(short)) throw new Exception("Key 'PROGRAM-TASKS' has already been set.");
                            _programTasks = (short) _convertProgramSetting(commands[1]);
                            break;
                        case "PROGRAM-PROCESSORS":
                            if (_programProcessors != default(short)) throw new Exception("Key 'PROGRAM-PROCESSORS' has already been set.");
                            _programProcessors = (short) _convertProgramSetting(commands[1]);
                            break;
                        case "RUNTIME-REFERENCE-FREQUENCY":
                            _setRuntimeRefFrequency(commands[1]);
                            break;
                        case "TASK-ID":
                            currentline = configurationStream.ReadLine();
                            while (!String.IsNullOrEmpty(currentline))
                            {
                                count++;
                                try
                                {
                                    _tasks.Add(new Task(currentline, _runtimeRefFrequency));
                                }
                                catch (Exception exc)
                                {
                                    _handleError(new Exception($"{exc.Message}. ({count})"));
                                }
                                finally
                                {
                                    currentline = configurationStream.ReadLine();
                                }
                            }
                            break;
                        case "PROCESSOR-ID":
                            currentline = configurationStream.ReadLine();
                            while (!String.IsNullOrEmpty(currentline))
                            {
                                count++;
                                try
                                {
                                    _processors.Add(new Processor(currentline));
                                }
                                catch (Exception exc)
                                {
                                    _handleError(new Exception($"{exc.Message}. ({count})"));
                                }
                                finally
                                {
                                    currentline = configurationStream.ReadLine();
                                }
                            }
                            break;
                        case "COEFFICIENT-ID":
                            currentline = configurationStream.ReadLine();
                            while (!String.IsNullOrEmpty(currentline))
                            {
                                count++;
                                try
                                {
                                    _setCoefficient(currentline);
                                }
                                catch (Exception exc)
                                {
                                    _handleError(new Exception($"{exc.Message}. ({count})"));
                                }
                                finally
                                {
                                    currentline = configurationStream.ReadLine();
                                }
                            }
                            break;
                        default:
                            /*Matches suitable empty lines*/
                            Regex empty = new Regex(@"^\s*(?!.)$");
                            /*Matches suitable comment lines*/
                            Regex comment = new Regex(@"^\s*\/\/.*$");
                            /*Matches unsuitable comment lines*/
                            Regex invalidComment = new Regex(@"^(\S)+(\t| *)\/\/$");
                            if (invalidComment.IsMatch(currentline))
                            {
                                _handleError(new Exception($"Invalid Comment ({count})"));
                            }
                            if (!comment.IsMatch(currentline) && !empty.IsMatch(currentline))
                            {
                                _handleError(new Exception($"Invalid line: {commands[0]}. ({count})"));
                            }
                            break;
                    }
                }
                catch (Exception exc)
                {
                    /*Add line number for traceability*/
                    _handleError(new Exception($"{exc.Message}. ({count})\n"));
                }
                count++;
            }
        }

        /*Configuration limits*/
        private void _setLimitTasks(string min, string max)
        {
            int[] limitTasks = new int[LIMIT_CAPACITY];
            if (_limitTasks != default(int[])) throw new Exception("Key 'LIMITS-TASKS' has already been set.");
            if (!int.TryParse(min, out limitTasks[0])) throw new Exception($"Invalid minimum for task limits: {min}");
            if (!int.TryParse(max, out limitTasks[1])) throw new Exception($"Invalid maximum for task limits: {max}");
            _limitTasks = limitTasks;
        }
        private void _setLimitProcessors(string min, string max)
        {
            int[] limitProcessors = new int[LIMIT_CAPACITY];
            if (_limitProcessors != default(int[])) throw new Exception("Key 'LIMITS-PROCESSORS' has already been set.");
            if (!int.TryParse(min, out limitProcessors[0])) throw new Exception($"Invalid minimum for processor limits: {min}");
            if (!int.TryParse(max, out limitProcessors[1])) throw new Exception($"Invalid maximum for processor limits: {max}");
            _limitProcessors = limitProcessors;
        }
        private void _setLimitFrequencies(string min, string max)
        {
            float[] limitFrequencies = new float[LIMIT_CAPACITY];
            if (_limitFrequencies != default(float[])) throw new Exception("Key 'LIMITS-TASKS' has already been set.");
            if (!float.TryParse(min, out limitFrequencies[0])) throw new Exception($"Invalid minimum for frequency limits: {min}");
            if (!float.TryParse(max, out limitFrequencies[1])) throw new Exception($"Invalid maximum for frequency limits: {max}");
            _limitFrequencies = limitFrequencies;
        }

        /*Program settings*/
        private float _convertProgramSetting(string programSetting)
        {
            float result;
            if (String.IsNullOrEmpty(programSetting)) throw new Exception("Invalid empty setting for program");
            if (!float.TryParse(programSetting, out result)) throw new Exception("Invalid value for program setting");
            return result;
        }
        /*Configuration settings methods*/
        private void _setRuntimeRefFrequency(string runtimeRefFrequency)
        {
            float result = default(float);
            if (_runtimeRefFrequency != default(float)) throw new Exception("Key 'RUNTIME-REFERENCE-FREQUENCY' has already been set.");
            if (String.IsNullOrEmpty(runtimeRefFrequency)) throw new Exception("Invalid empty runtime reference frequency");
            if (!float.TryParse(runtimeRefFrequency, out result)) throw new Exception("Invalid value for runtime reference frequency");
            _runtimeRefFrequency = result;
        }

        private void _setCoefficient(string line)
        {
            if (String.IsNullOrEmpty(line)) throw new Exception("Invalid coefficient, data missing");
            string[] pair = line.Split(',');
            short iD;
            float value;
            if (short.TryParse(pair[0], out iD) && float.TryParse(pair[1], out value))
            {
                _coefficients[iD] = value;
            }
            else
            {
                throw new Exception("Invalid data type for coefficient");
            }
        }

        private void _validateConfig()
        {
            /*Properties to validate*/
            if (String.IsNullOrEmpty(_logfileName)) _handleError(new Exception("Key 'DEFAULT-LOGFILE' missing from configuration"));
            if (_limitTasks == default(int[])) _handleError(new Exception("Key 'LIMITS-TASKS' missing from configuration"));
            if (_limitProcessors == default(int[])) _handleError(new Exception("Key 'LIMITS-PROCESSORS' missing from configuration"));
            if (_limitFrequencies == default(float[])) _handleError(new Exception("Key 'LIMITS-PROCESSOR-FREQUENCIES' missing from configuration"));
            if (_programMaxDuration == default(int)) _handleError(new Exception("Key 'PROGRAM-MAXIMUM-DURATION' missing from configuration")); ;
            if (_programTasks == default(int)) _handleError(new Exception("Key 'PROGRAM-TASKS' missing from configuration"));
            if (_programProcessors == default(int)) _handleError(new Exception("Key 'PROGRAM-PROCESSORS' missing from configuration"));
            if (_runtimeRefFrequency == default(float)) _handleError(new Exception("Key 'RUNTIME-REFERENCE-FREQUENCY' missing from configuration"));
            if (_tasks.Count == 0) _handleError(new Exception("Key 'TASK-ID,RUNTIME' missing from configuration"));
            if (_processors.Count == 0) _handleError(new Exception("Key 'PROCESSOR-ID,FREQUENCY' missing from configuration"));
            if (_coefficients.Length == 0) _handleError(new Exception("Key 'COEFFICIENT-ID,VALUE' missing from configuration"));
        }

        private void _handleError(Exception exc)
        {
            /*Handles internal exceptions by amalgamating into an invalidity report*/
            StringBuilder sb = new StringBuilder();
            sb.Append(_invalidityReport);
            sb.AppendLine(exc.Message);
            _invalidityReport = sb.ToString();
        }
        /*Public methods for */
        public bool isValid()
        {
            return String.IsNullOrEmpty(_invalidityReport);
        }

        public string reportErrors()
        {
            return _invalidityReport;
        }
    }
}
