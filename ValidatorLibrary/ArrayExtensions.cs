﻿using System.Collections.Generic;

namespace ValidatorLibrary
{
    public static class ArrayExtensions
    {
        public static IEnumerable<T> GetAllocationTuple<T>(this T[,] array, int rowIndex)
        {
            int columnsCount = array.GetLength(1);
            for (int colIndex = 1; colIndex < columnsCount; colIndex++)
                yield return array[rowIndex, colIndex];
        }
    }
}