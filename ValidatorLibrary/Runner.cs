﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
/*
* Assignment 2 
* Cloud Application Development
* Andrew Bellamy - 215240036
* SIT323
*/
namespace ValidatorLibrary
{
    public class Runner
    {
        private Configuration _config;

        public Runner(Configuration config)
        {
            _config = config;
        }

        /*Process Methods*/
        public string runAllocation(Allocation allocation)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.Append(_processAllocation(allocation));
            return sb.ToString();
        }
        /*Retrieves the process of the allocation*/
        private string _processAllocation(Allocation allocation)
        {
            string time = _config != null ? calculateTime(allocation).ToString("F2") : "Invalid Time";
            string energy = _config != null ? calculateEnergy(allocation).ToString("F2") : "Invalid Energy";

            StringBuilder sb = new StringBuilder();
            sb.AppendLine();

            if (allocation.isValid)
                sb.AppendLine($"Allocation ID: {allocation.ID}, Time: {time}, Energy: {energy}");
            else
                sb.AppendLine($"Allocation ID: {allocation.ID} is invalid.");

            foreach (int[] processor in allocation.processors)
            {
                sb.AppendLine(string.Join(",", processor));
            }
            return sb.ToString();
        }
        /*Process/Calculate allocation*/
        /*Max time*/
        public float calculateTime(Allocation allocation)
        {
            List<float> results = new List<float>();
            for (short s = 0; s < allocation.processors.Count; s++)
            {
                results.Add(_getTimeOfProcessor(s, allocation.processors[s]));
            }
            return results.Max();
        }
        /*Iterate over processors to calculate tasks*/
        private float _getTimeOfProcessor(short index, int[] processor)
        {
            float result = default(float);
            for (short s = 0; s < processor.Length; s++)
            {
                /*Calculate time based on distance / speed, using indexes of tasks and processor in the config*/
                if (processor[s] == 1) result += _config.tasks[s].Distance / _config.processors[index].Frequency;
            }
            return result;
        }
        /*Energy*/
        public float calculateEnergy(Allocation allocation)
        {
            float result = default(float);
            short index = 0;
            foreach (int[] processor in allocation.processors)
            {
                result += _getEnergyOfProcessor(index, processor);
                index++;
            }
            return result;
        }
        /*Iterate over processors to calculate tasks*/
        private float _getEnergyOfProcessor(short index, int[] processor)
        {
            float result = default(float);
            for (int i = 0; i < processor.Length; i++)
            {
                if (processor[i] == 1) result += _getEnergyOfTask(_config.processors[index].Frequency, i);
            }
            return result;
        }
        /*Perform caluclations using task and processor associations*/
        private float _getEnergyOfTask(float frequency, int task)
        {
            float quadratic = _config.coefficients[2] * (frequency * frequency) + (_config.coefficients[1] * frequency) + _config.coefficients[0];
            float result = quadratic * (_config.tasks[task].Distance / frequency);
            return result;
        }
    }
}