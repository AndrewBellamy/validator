﻿using System;
/*
 * Assignment 2 
 * Cloud Application Development
 * Andrew Bellamy - 215240036
 * SIT323
 */
namespace ValidatorLibrary
{
    public class Processor
    {
        private short _id;
        private float _frequency;
        /*Read only properties*/
        public short ID { get => _id; }
        public float Frequency { get => _frequency; }
        /*Simple constructor*/
        public Processor(string line)
        {
            if (String.IsNullOrEmpty(line)) throw new Exception("Invalid processor, data missing");
            string[] pair = line.Split(',');
            if (!short.TryParse(pair[0], out _id) || !float.TryParse(pair[1], out _frequency)) throw new Exception("Invalid data type for processor or frequency");
        }
    }
}
