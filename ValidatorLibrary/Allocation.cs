﻿using System;
using System.Collections.Generic;
using System.Text;
/*
 * Assignment 2 
 * Cloud Application Development
 * Andrew Bellamy - 215240036
 * SIT323
 */
namespace ValidatorLibrary
{
    public class Allocation
    {
        private string _id;
        private short _maxTasks;
        private short _maxProcessors;
        private List<int[]> _processors;
        private StringBuilder _invalidationReport;
        private bool _valid;

        public string ID { get => _id; }
        public bool isValid { get => _valid; }
        public List<int[]> processors { get => _processors; }

        public Allocation(string id, string[] lines, short maxTasks, short maxProcessors)
        {
            _invalidationReport = new StringBuilder();
            _id = id;
            _processors = new List<int[]>();
            _maxTasks = maxTasks;
            _maxProcessors = maxProcessors;
            _createMatrix(lines);
            /*Lastly, perform a validation*/
            _validate();
        }
        /*Sets allocation collections*/
        private void _createMatrix(string[] lines)
        {
            for (short s = 0; s < _maxProcessors; s++)
            {
                if (String.IsNullOrEmpty(lines[s])) throw new Exception("Invalid processor allocation, data missing");
                string[] binaries = lines[s].Split(',');
                _processors.Add(_setRows(binaries));
            }
        }

        private int[] _setRows(string[] binaries)
        {
            int[] numBinaries = new int[_maxTasks];

            for (int i = 0; i < binaries.Length; i++)
            {
                if (!int.TryParse(binaries[i], out numBinaries[i])) throw new Exception($"Invalid data type for task allocation binary {binaries[i]}");
            }
            return numBinaries;
        }
        /*Sets whether allocation is valid, based on allocation matrix values*/
        private void _validate()
        {
            bool isValid = true;
            for (int i = 0; i < _maxTasks; i++)
            {
                int result = 0;
                for (int j = 0; j < _maxProcessors; j++)
                {
                    result += _processors[j][i];
                }
                if (result != 1) _invalidationReport.AppendLine($"Invalid: a task, ID: {(i + 1)}, in allocation {_id} has been allocated to {result} processors instead of 1\n");
                isValid = isValid && result == 1;
            }
            _valid = isValid;
        }
        /*Retrieve the error report*/
        public string reportErrors()
        {
            if (!_valid) _invalidationReport.AppendLine($"Allocation: {_id}, is invalid.");
            return _invalidationReport.ToString();
        }
    }
}
