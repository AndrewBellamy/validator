﻿using System;
/*
 * Assignment 2 
 * Cloud Application Development
 * Andrew Bellamy - 215240036
 * SIT323
 */
namespace ValidatorLibrary
{
    public class Task
    {
        private short _id;
        private float _distance;
        /*Read only property*/
        public float Distance { get => _distance; }
        /*Constructor*/
        public Task(string line, float runtimeRefFrequency)
        {
            if (String.IsNullOrEmpty(line)) throw new Exception("Invalid task, data missing");
            string[] pair = line.Split(',');
            float runtime;
            if (short.TryParse(pair[0], out _id) && float.TryParse(pair[1], out runtime))
            {
                _distance = runtime * runtimeRefFrequency;
            }
            else
            {
                throw new Exception("Invalid data type for task or runtime distance");
            }
        }

    }
}
