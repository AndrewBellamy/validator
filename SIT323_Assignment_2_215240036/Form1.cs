﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Timers;
using System.Linq;
using System.ComponentModel;
using ValidatorLibrary;
using System.Collections.Generic;
using System.Threading.Tasks;
/*
* Assignment 2 
* Cloud Application Development
* Andrew Bellamy - 215240036
* SIT323
*/
namespace SIT323_Assignment_2_215240036
{
    public partial class Validation : Form
    {
        /*Project directory, over working directory*/
        private string directory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
        private TestProgram _testProgram;

        /*Additions for Assignment 2 Enhancements*/
        private const Double _timeout = 180000;
        private const int _oneSecond = 1000;

        private string _url;
        private Configuration _config;
        private Runner _allocationRunner;
        private BackgroundWorker allocationCaller;
        private bool _allocationRun;
        private delegate void SafeCallDelegate(string output, bool clear);
        System.Windows.Forms.Timer _countdownTimer;
        private double _remaining;
        private string _loggedEntries;

        /*Services*/
        LocalService.ServiceClient _GRsvc;
        CoinChangeService.ServiceClient _CCsvc;
        RandomService.ServiceClient _BFsvc;

        /*AWS*/
        //AWSGreedyService.ServiceClient _GRsvc;
        //AWSCoinService.ServiceClient _CCsvc;
        //AWSBruteService.ServiceClient _BFsvc;

        public Validation()
        {
            InitializeComponent();
        }

        private void On_go(Object sender, EventArgs args)
        {
            /*Deferring to handler for re-use*/
            _startAllocationProcessing();
        }

        private void On_UrlKeypress(Object sender, KeyPressEventArgs args)
        {
            /*Keypress to call handler on enter*/
            if (args.KeyChar == 13) _startAllocationProcessing();
        }

        private void On_Cancel(Object sender, EventArgs args)
        {
            /*Cancels the running job and countdown*/
            if (allocationCaller.IsBusy && !allocationCaller.CancellationPending)
            {
                allocationCaller.CancelAsync();
                _allocationRun = true;
                _countdownComplete();
            }
        }

        private void _startAllocationProcessing()
        {
            try
            {
                /*Disable controls and gather test url*/
                Button goButton = this.Controls["goButton"] as Button;
                goButton.Enabled = false;
                ComboBox inputControl = this.Controls["ConfigURL"] as ComboBox;
                _url = $"{inputControl.Text}";
                inputControl.Enabled = false;

                /*Create a clientside configuration and allocation runner*/
                _config = new Configuration(_url, true);
                _allocationRunner = new Runner(_config);

                /*Clear output window and begin background allocation service calls*/
                _clearOutput();
                allocationCaller = new BackgroundWorker();
                allocationCaller.DoWork += new DoWorkEventHandler(_serviceAllocations);
                allocationCaller.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_serviceCompleted);
                allocationCaller.WorkerSupportsCancellation = true;
                allocationCaller.RunWorkerAsync();

                /*Reset the countdown*/
                _remaining = _timeout;
                _updateCountdown();
                /*Countdown timer*/
                _countdownTimer = new System.Windows.Forms.Timer();
                _countdownTimer.Tick += new EventHandler(_countdown);
                _countdownTimer.Interval = _oneSecond;
                _countdownTimer.Start();

                /*Enable cancel*/
                Button cancelButton = this.Controls["cancelButton"] as Button;
                cancelButton.Enabled = true;
            } catch(Exception exc)
            {
                MessageBox.Show(exc.Message, "Fatal", MessageBoxButtons.OK);
                goButton.Enabled = true;
            }
        }

        private void _serviceAllocations(Object sender, DoWorkEventArgs args)
        {
            /*Attempts to call the service, errors are printed in output window*/
            try
            {
                _GRsvc = new LocalService.ServiceClient();
                _CCsvc = new CoinChangeService.ServiceClient();
                _BFsvc = new RandomService.ServiceClient();
                //_GRsvc = new AWSGreedyService.ServiceClient();
                //_CCsvc = new AWSCoinService.ServiceClient();
                //_BFsvc = new AWSBruteService.ServiceClient();
                _allocationRun = false;
                /*Default time is set by global*/
                System.Timers.Timer timer = new System.Timers.Timer(_timeout);
                timer.AutoReset = false;
                timer.Elapsed += (Object source, ElapsedEventArgs a) => { _allocationRun = true; };
                timer.Start();
                while (!_allocationRun)
                {
                    Parallel.Invoke(
                        () => _readAllocation(_GRsvc.GenerateAllocation(_url), "Greedy Algorithm"),
                        () => _readAllocation(_BFsvc.GenerateAllocation(_url), "Brute Force Algorithm"),
                        () => _readAllocation(_CCsvc.GenerateAllocation(_url), "Coin Change Algorithm")
                    );
                }
            } catch (Exception exc)
            {
                _writeToOutput(exc.Message, true);
                allocationCaller.CancelAsync();
            }
        }

        /*Judges new allocations for respective algorithm as they come in*/
        private void _readAllocation(string allocationMatrix, string source)
        {
            if (allocationMatrix.Split('\n').Length <= 2)
            {
                _log(allocationMatrix);
                return;
            }
            StringReader sr = new StringReader(allocationMatrix);
            /*First line should be the service IP Address*/
            _log(sr.ReadLine());
            /*Read in the allocation stream and create the allocation*/
            List<string> lines = new List<string>();
            while (true)
            {
                string line = sr.ReadLine();
                if (line == null) break;
                if (line == "") continue;
                lines.Add(line);
            }
            Allocation newAllocation = new Allocation(source, lines.ToArray(), _config.programTasks, _config.programProcessors);
            _writeToOutput(_allocationRunner.runAllocation(newAllocation), false);
        }

        private void _serviceCompleted(Object sender, RunWorkerCompletedEventArgs args)
        {
            /*Output completion message and enabled controls*/
            _writeToOutput("\nAllocation Service Finished" , false);
            _writeToLogFile(_loggedEntries);
            _allocationRunner = null;
            /*Re-enabling the controls*/
            Button goButton = this.Controls["goButton"] as Button;
            goButton.Enabled = true;
            ComboBox inputControl = this.Controls["ConfigURL"] as ComboBox;
            inputControl.Enabled = true;
            /*Clearing the log*/
            _loggedEntries = "";
            _countdownComplete();
        }

        /*handler for the secondary timer ticks*/
        private void _countdown(Object sender, EventArgs args)
        {
            _remaining -= _oneSecond;
            if (_remaining > 0)
                _updateCountdown();
            else
                _countdownComplete();
        }

        /*Updating the countdown label*/
        private void _updateCountdown()
        {
            string remaining = TimeSpan.FromMilliseconds(_remaining).ToString(@"mm\:ss");
            Label countdown = this.Controls["countdown"] as Label;
            countdown.Text = remaining;
        }

        /*Clearing can disposing of the countdown timer and label*/
        private void _countdownComplete()
        {
            _countdownTimer.Stop();
            _countdownTimer.Dispose();
            /*Clear countdown label*/
            Label countdown = this.Controls["countdown"] as Label;
            countdown.Text = "";
            /*Disable cancel*/
            Button cancelButton = this.Controls["cancelButton"] as Button;
            cancelButton.Enabled = false;
        }

        /*Better for async instead of accessing the file multiple times*/
        private void _log(string entry = "")
        {
            _loggedEntries += $"\n{entry}\n";
        }

        /*Writing to external file*/
        private async void _writeToLogFile(string process)
        {
            if (_config != null && !String.IsNullOrEmpty(_config.logfilename))
            {
                string parent = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
                string filepath = $"{parent}\\{_config.logfilename}";
                StreamWriter streamOut;
                try
                {
                    streamOut = new StreamWriter(filepath, append: true);
                    await streamOut.WriteLineAsync($"Log taken: {DateTime.Now.ToString()}");
                    await streamOut.WriteLineAsync(process);
                    streamOut.Close();
                }
                catch (Exception exc)
                {
                    MessageBox.Show($"Fatal logging error: {exc.Message}\n", "Fatal", MessageBoxButtons.OK);
                }
            }
        }

        private void Open_File(object sender, EventArgs e)
        {
            if (allocationCaller != null && allocationCaller.IsBusy)
            {
                MessageBox.Show("Cancel the running process first, or wait until process ends.", "Warning", MessageBoxButtons.OK);
                return;
            }
            /*Dump test program and config when loading a new one*/
            _testProgram = null;
            var fileContent = string.Empty;
            var filePath = string.Empty;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = directory;
                openFileDialog.Multiselect = false;
                openFileDialog.Filter = "tan files (*.tan)|*.tan";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    /*Set open directory to recent*/
                    filePath = openFileDialog.FileName;
                    directory = Path.GetDirectoryName(filePath);
                    /*Setup Allocation + Configuration*/
                    var stream = openFileDialog.OpenFile();
                    FileStream fileStream = stream as FileStream;
                    if (fileStream != null)
                    {
                        _testProgram = new TestProgram(fileStream);
                        fileContent = _testProgram.ToString();
                    } else
                    {
                        fileContent = "Invalid or corrupted file";
                    }
                    _writeToOutput(fileContent, true);
                } else
                {
                    //File Dialog Cancelled
                    Debug.WriteLine("File Upload Cancelled");
                }
            }
        }
        /*Clears output*/
        private void _clearOutput()
        {
            RichTextBox outputControl = this.Controls["TestProgramOutput"] as RichTextBox;
            outputControl.Clear();
        }

        /*Handles writing to the main page*/
        private void _writeToOutput(string output, bool clear)
        {

            RichTextBox outputControl = this.Controls["TestProgramOutput"] as RichTextBox;
            if (outputControl.InvokeRequired)
            {
                var d = new SafeCallDelegate(_writeToOutput);
                outputControl.Invoke(d, new Object[] { output, clear });
            } else
            {
                if (clear) outputControl.Clear();
                outputControl.AppendText(output);
            }
        }
        /*Displays the error report*/
        private void On_Error_Report(object sender, EventArgs e)
        {
            if (_checkProgramExists()) MessageBox.Show(_testProgram.reportErrors(), "Error Report", MessageBoxButtons.OK);
        }
        /*Some functions are dependent on testProgram*/
        private bool _checkProgramExists()
        {
            if (_testProgram != null) return true;
            MessageBox.Show("A test program has not been opened yet.", "Warning", MessageBoxButtons.OK);
            return false;
        }
        /*Credits*/
        private void On_Help_about(Object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Validator - SIT323 Assignment 1\n");
            sb.AppendLine("Validates test program and configuration files, and calculates the energy and runtime of task to processor allocations.\n");
            sb.AppendLine("Created by Andrew Bellamy | 215240036");
            sb.AppendLine("24 August 2019");
            sb.AppendLine("\nSIT323 Assignment 2 - v1.1 Enhancements\n");
            sb.AppendLine("Uses cloud instance services to generate allocations based on test configurations\n");
            sb.AppendLine("Updated by Andrew Bellamy | 215240036");
            sb.AppendLine("30 September 2019");
            MessageBox.Show(sb.ToString(), "About Validator", MessageBoxButtons.OK);
        }
        /*Close the Validator Program*/
        private void Exit_Program(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
