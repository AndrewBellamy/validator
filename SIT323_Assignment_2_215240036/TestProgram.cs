﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using ValidatorLibrary;
/*
 * Assignment 2 
 * Cloud Application Development
 * Andrew Bellamy - 215240036
 * SIT323
 */
namespace SIT323_Assignment_2_215240036
{
   public class TestProgram
    {
        private Configuration _config;
        private string _filepath;
        private short _tasks;
        private short _processors;
        private short _allocationLimit;
        private List<Allocation> _allocations;
        private string _log;
        private List<float> _energies;

        public bool isValid { get => String.IsNullOrEmpty(_log); }

        public TestProgram(FileStream filestream)
        {
            StreamReader allocationStream;
            try
            {
                allocationStream = new StreamReader(filestream);
            }
            catch (IOException exc)
            {
                throw exc;
            }
            _filepath = Path.GetDirectoryName(filestream.Name);
            _allocations = new List<Allocation>();
            _energies = new List<float>();
            _setupAllocation(allocationStream);
            /*Close stream to free-up system resource*/
            allocationStream.Close();
            _validate();
        }
        /*Setup methods*/
        private void _setupAllocation(StreamReader allocationStream)
        {
            int count = 0;
            while (!allocationStream.EndOfStream)
            {
                string currentline = allocationStream.ReadLine();
                try
                {
                    string[] commands = currentline.Split(',');
                    switch (commands[0])
                    {
                        case "CONFIGURATION":
                            /*Create and load configuration object*/
                            if (_config != null) throw new Exception("Key 'CONFIGURATION' has already been set.");
                            _config = new Configuration($"{_filepath}\\{commands[1].Replace("\"", "")}", false);
                            break;
                        case "TASKS":
                            if (_tasks != default(int)) throw new Exception("Key 'TASKS' has already been set.");
                            _tasks = _setTasksPerAllocation(commands[1]);
                            break;
                        case "PROCESSORS":
                            if (_processors != default(int)) throw new Exception("Key 'PROCESSORS' has already been set.");
                            _processors = _setProcessorsPerAllocation(commands[1]);
                            break;
                        case "ALLOCATIONS":
                            if (_allocationLimit != default(int)) throw new Exception("Key 'ALLOCATIONS' has already been set.");
                            _allocationLimit = _setAllocationLimit(commands[1]);
                            break;
                        case "ALLOCATION-ID":
                            string allocationID = commands[1];
                            string[] tasksPerProcessor = new string[_processors];
                            for (int i = 0; i < _processors; i++)
                            {
                                count++;
                                tasksPerProcessor[i] = (allocationStream.ReadLine());
                            }
                            try
                            {
                                Allocation newAllocation = new Allocation(allocationID, tasksPerProcessor, _tasks, _processors);
                                if (!newAllocation.isValid) _logItem($"{newAllocation.reportErrors()}");
                                _allocations.Add(newAllocation);
                            } catch (Exception exc)
                            {
                                _logItem($"{exc.Message}. ({count})");
                            }
                            break;
                        default:
                            /*Matches suitable empty lines*/
                            Regex empty = new Regex(@"^\s*(?!.)$");
                            /*Matches suitable comment lines*/
                            Regex comment = new Regex(@"^\s*\/\/.*$");
                            /*Matches unsuitable comment lines*/
                            Regex invalidComment = new Regex(@"^(\S)+(\t| *)\/\/$");
                            if (invalidComment.IsMatch(currentline))
                            {
                                _logItem($"Invalid Comment ({count})\n");
                            }
                            if (!comment.IsMatch(currentline) && !empty.IsMatch(currentline))
                            {
                                _logItem($"Invalid line: {commands[0]}. ({count})\n");
                            }
                            break;
                    }
                }
                catch (Exception exc)
                {
                    /*Add line number for traceability*/
                    _logItem($"Exception: {exc.Message}. (At line {count})");
                }
                count++;
            }
        }
        /*Sets tasks per allocation*/
        private short _setTasksPerAllocation(string tasksSetting)
        {
            short result;
            if (String.IsNullOrEmpty(tasksSetting)) throw new Exception("Invalid empty setting for tasks per allocation");
            if (!short.TryParse(tasksSetting, out result)) throw new Exception("Invalid value for tasks per allocation");
            return result;
        }
        /*Sets processors per allocation*/
        private short _setProcessorsPerAllocation(string processorsSetting)
        {
            short result;
            if (String.IsNullOrEmpty(processorsSetting)) throw new Exception("Invalid empty setting for processors per allocation");
            if (!short.TryParse(processorsSetting, out result)) throw new Exception("Invalid value for processors per allocation");
            return result;
        }
        /*Sets allocations limit*/
        private short _setAllocationLimit(string allocationSetting)
        {
            short result;
            if (String.IsNullOrEmpty(allocationSetting)) throw new Exception("Invalid empty setting for allocation limit");
            if (!short.TryParse(allocationSetting, out result)) throw new Exception("Invalid value for allocation limit");
            return result;
        }
        /*validate the set program*/
        private void _validate()
        {
            /*Properties to validate*/
            if (_allocations.Count != _allocationLimit) _logItem($"The number of allocations ({_allocations.Count}) is not equal to the expected number ({_allocationLimit}).\n");
            if (_config == null) _logItem("Key 'CONFIGURATION' missing from program\n");
            if (_tasks == default(short)) _logItem("Key 'TASKS' missing from program\n");
            if (_processors == default(short)) _logItem("Key 'PROCESSORS' missing from program\n");
            if (_allocationLimit == default(short)) _logItem("Key 'ALLOCATIONS' missing from program\n");
        }

        /*Process Methods*/
        private string _runAllocations()
        {
            StringBuilder sb = new StringBuilder();
            foreach (Allocation allocation in _allocations)
            {
                sb.AppendLine();
                sb.Append(_processAllocation(allocation));
            }
            return sb.ToString();
        }
        /*Retrieves the process of the allocation*/
        private string _processAllocation(Allocation allocation)
        {
            string time = _config != null ? _calculateTime(allocation).ToString("F2") : "Invalid Time";
            string energy = _config != null ? _calculateEnergy(allocation).ToString("F2") : "Invalid Energy";

            StringBuilder sb = new StringBuilder();
            sb.AppendLine();

            if (allocation.isValid)
                sb.AppendLine($"Allocation ID: {allocation.ID}, Time: {time}, Energy: {energy}");
            else
                sb.AppendLine($"Allocation ID: {allocation.ID} is invalid.");

            foreach (int[] processor in allocation.processors)
            {
                sb.AppendLine(string.Join(",", processor));
            }
            return sb.ToString();
        }
        /*Process/Calculate allocation*/
        /*Max time*/
        private float _calculateTime(Allocation allocation)
        {
            List<float> results = new List<float>();
            for (short s = 0; s < allocation.processors.Count; s++)
            {
                results.Add(_getTimeOfProcessor(s,allocation.processors[s]));
            }
            /*Each processor processes simultaneously, we only need the max*/
            if (results.Max() > _config.programMaxDuration)
                _logItem($"The runtime ({results.Max():F2}) of an allocation (ID: {allocation.ID}) exceeds the maximum runtime limit ({_config.programMaxDuration:F2})\n");
            return results.Max();
        }
        /*Iterate over processors to calculate tasks*/
        private float _getTimeOfProcessor(short index, int[] processor)
        {
            float result = default(float);
            for (short s = 0; s < processor.Length; s++)
            {
                /*Calculate time based on distance / speed, using indexes of tasks and processor in the config*/
                if (processor[s] == 1) result += _config.tasks[s].Distance / _config.processors[index].Frequency;
            }
            return result;
        }
        /*Energy*/
        private float _calculateEnergy(Allocation allocation)
        {
            float result = default(float);
            short index = 0;
            foreach (int[] processor in allocation.processors)
            {
                result += _getEnergyOfProcessor(index, processor);
                index++;
            }
            _validateEnergy(result);
            return result;
        }
        /*Iterate over processors to calculate tasks*/
        private float _getEnergyOfProcessor(short index, int[] processor)
        {
            float result = default(float);
            for (int i = 0; i < processor.Length; i++)
            {
                if (processor[i] == 1) result += _getEnergyOfTask(_config.processors[index].Frequency, i);
            }
            return result;
        }
        /*Perform caluclations using task and processor associations*/
        private float _getEnergyOfTask(float frequency, int task)
        {
            float quadratic = _config.coefficients[2] * (frequency * frequency) + (_config.coefficients[1] * frequency) + _config.coefficients[0];
            float result = quadratic * (_config.tasks[task].Distance / frequency);
            return result;
        }
        /*Adds an entry to the log, if incomming energy is different*/
        private void _validateEnergy(float energy)
        {
            int index = _energies.FindIndex((x) => x != energy);
            if (index != -1) _logItem($"The energy ({energy:F2}) of allocation {_energies.Count + 1}, is different to the energy of allocation {index + 1} ({_energies[index]:F2}).\n");
            _energies.Add(energy);
        }
        /*Adding error messages to the log*/
        private void _logItem(string log)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(_log);
            sb.AppendLine(log);
            _log = sb.ToString();
        }
        /*Error report; essentially the log*/
        public string reportErrors()
        {
            /*Pull all the errors together*/
            StringBuilder sb = new StringBuilder();
            if (_config != null) sb.Append(_config.reportErrors());
            sb.AppendLine(_log);
            return sb.ToString();
        }
        /*Output of TAN and CSV validity*/
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Test Program is {0}.\n", String.IsNullOrEmpty(_log) ? "valid" : "invalid");
            sb.AppendFormat("\nConfiguration is {0}.", (_config != null && _config.isValid()) ? "valid" : "invalid");
            string process = _runAllocations();
            sb.AppendLine(process);
            _writeToLogFile(process); /*Performing the log, once everything has been set and run*/
            return sb.ToString();
        }

        /*Writing to external file*/
        private void _writeToLogFile(string process)
        {
            if (_config != null && !String.IsNullOrEmpty(_config.logfilename))
            {
                string filepath = $"{_filepath}\\{_config.logfilename}";
                StreamWriter streamOut;
                try
                {
                    streamOut = new StreamWriter(filepath, append: true);
                    streamOut.WriteLine($"Log taken: {DateTime.Now.ToString()}");
                    streamOut.WriteLine(process);
                    streamOut.Write(reportErrors());
                    streamOut.Close();
                }
                catch (Exception exc)
                {
                    _logItem($"Fatal error: {exc.Message}\n");
                }
            } else
            {
                /*This will be logged rather than raising an exception*/
                _logItem("Cannot write log to external file. The log filename is unset.\n");
            }
        }
    }
}
