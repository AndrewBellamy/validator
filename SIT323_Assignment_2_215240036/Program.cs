﻿using System;
using System.Windows.Forms;
/*
 * Assignment 2 
 * Cloud Application Development
 * Andrew Bellamy - 215240036
 * SIT323
 */
namespace SIT323_Assignment_2_215240036
{
    static class Program
    {
        [STAThread]
        static void Main()
       {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Validation());
        }
    }
}
